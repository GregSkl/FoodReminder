package com.gregskl.foodreminder;

/**
 * Created by Gregory on 11-Sep-16.
 */
public class Food {

    private String text;
    private boolean available;

    public Food(String text, boolean available) {
        this.text = text;
        this.available = available;
    }

    public String getText() {
        return text;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
