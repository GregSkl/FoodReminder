package com.gregskl.foodreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by Gregory on 16-Oct-16.
 */

public class AutoStart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        if(sharedPrefs.getBoolean("enabled", true)) {
            Intent i = new Intent(context, FoodService.class);
            context.startService(i);
        }
    }
}
